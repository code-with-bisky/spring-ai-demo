package com.codewithbisky.ai;

import com.codewithbisky.ai.service.AIChatbot;
import com.codewithbisky.ai.service.OpenAiServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class DemoAiApplication implements CommandLineRunner {

	private final OpenAiServiceImpl openAiService;
	private final AIChatbot chatbot;
	public static void main(String[] args) {
		SpringApplication.run(DemoAiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


//		openAiService.embeddingExample();

//		openAiService.pdfDocumentEtl();


//		String chat = chatbot.chat("WHEN CHILDREN SHOULD BE EXCLUDED OR DISMISSED FROM A CHILDCARE SETTING");
//
//		System.out.println("Response : "+chat);

	}
}
