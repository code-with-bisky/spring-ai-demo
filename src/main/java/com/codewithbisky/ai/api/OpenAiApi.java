package com.codewithbisky.ai.api;

import com.codewithbisky.ai.service.AIChatbot;
import com.codewithbisky.ai.service.OpenAiServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("open-ai")
@RequiredArgsConstructor
public class OpenAiApi {

    private final OpenAiServiceImpl openAiService;
    private final AIChatbot aiChatbot;



    @PostMapping("/question")
    public String chat(@RequestParam String question){

        return  openAiService.chatResult(question);
    }



    @PostMapping("/document-chat")
    public String documentChat(@RequestParam String question){

        return  aiChatbot.chat(question);
    }
}
