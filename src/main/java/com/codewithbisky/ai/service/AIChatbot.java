package com.codewithbisky.ai.service;


import lombok.RequiredArgsConstructor;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.SystemPromptTemplate;
import org.springframework.ai.document.Document;
import org.springframework.ai.openai.OpenAiChatClient;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AIChatbot {


    private final OpenAiChatClient chatClient;
    private final VectorStore vectorStore;


    public String chat(String message){

        List<Document> documents = vectorStore.similaritySearch(message);

        String docmentsStrings = documents.
                stream()
                .map(Document::getContent)
                .collect(Collectors.joining(System.lineSeparator()));



        String systemTemplateText = """
                 You are a helpful AI assistant that helps people questions about Disease for Childcare Providers
                  The New Hampshire Division of Public Health Services, Bureau of Infectious Disease
                  Control, prepared this manual for childcare providers and parents/guardians of children
                  attending childcare. The disease fact sheets, which comprise most of this document, are
                  intended to familiarize people with specific infectious disease problems commonly
                  encountered in childcare.
                  
                  Use the information from the DOCUMENTS section to provide an accurate answers but just act like you know more about this
                  
                  DOCUMENTS:
                  {documents}
                """;

        SystemPromptTemplate systemPromptTemplate = new SystemPromptTemplate(systemTemplateText);
        Message systemMessage = systemPromptTemplate.createMessage(Map.of("documents", docmentsStrings));
        Message userMessage = new UserMessage(message);
        Prompt prompt = new Prompt(List.of(systemMessage, userMessage));
        ChatResponse chatResponse = chatClient.call(prompt);
        return chatResponse.getResult().getOutput().getContent();
    }

}
