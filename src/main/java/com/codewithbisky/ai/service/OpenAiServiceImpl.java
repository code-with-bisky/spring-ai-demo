package com.codewithbisky.ai.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.document.Document;
import org.springframework.ai.embedding.Embedding;
import org.springframework.ai.embedding.EmbeddingRequest;
import org.springframework.ai.embedding.EmbeddingResponse;
import org.springframework.ai.openai.OpenAiChatClient;
import org.springframework.ai.openai.OpenAiChatOptions;
import org.springframework.ai.openai.OpenAiEmbeddingClient;
import org.springframework.ai.openai.OpenAiEmbeddingOptions;
import org.springframework.ai.reader.ExtractedTextFormatter;
import org.springframework.ai.reader.pdf.PagePdfDocumentReader;
import org.springframework.ai.reader.pdf.config.PdfDocumentReaderConfig;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.ai.vectorstore.SearchRequest;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class OpenAiServiceImpl {


    private final OpenAiEmbeddingClient openAiEmbeddingClient;
    private final OpenAiChatClient chatClient;
    private final VectorStore vectorStore;
    private final JdbcTemplate jdbcTemplate;

    public List<Double> embeddingExample() {

        EmbeddingResponse embeddingResponse = openAiEmbeddingClient.call(
                new EmbeddingRequest(List.of("Hello World", "World is big and salvation is near"),
                        OpenAiEmbeddingOptions.builder()
                                .withModel("text-embedding-ada-002")
                                .build()));

        List<Embedding> results = embeddingResponse.getResults();
        List<Double> embeddings = new ArrayList<>();
        results.forEach(embedding -> {

            List<Double> output = embedding.getOutput();
            embeddings.addAll(output);
        });

        log.info("Embeddings >> {}", embeddings);

        return embeddings;
    }

    public String chatResult(String question) {

        ChatResponse response = chatClient.call(
                new Prompt(
                        question,
                        OpenAiChatOptions.builder()
                                .withModel("gpt-3.5-turbo")
                                .withTemperature(0.4f)
                                .build()
                ));
        return response.getResult().getOutput().getContent();
    }


    public void saveEmbeddings() {

        List<Document> documents = List.of(
                new Document("Spring AI rocks!! Spring AI rocks!! Spring AI rocks!! Spring AI rocks!! Spring AI rocks!!", Map.of("meta1", "meta1")),
                new Document("The World is Big and Salvation Lurks Around the Corner"),
                new Document("You walk forward facing the past and you turn back toward the future.", Map.of("meta2", "meta2")));

        // Add the documents to PGVector
        vectorStore.add(documents);
        // Retrieve documents similar to a query
        List<Document> results = vectorStore.similaritySearch(SearchRequest.query("Spring").withTopK(5));


        System.out.println(results.get(0).getContent());
    }



    public void pdfDocumentEtl(){

        jdbcTemplate.update("delete  from vector_store");

        //extract read document
        PagePdfDocumentReader pdfReader = new PagePdfDocumentReader("classpath:/disease-handbook-complete.pdf",
                PdfDocumentReaderConfig.builder()
                        .withPageTopMargin(0)
                        .withPageExtractedTextFormatter(ExtractedTextFormatter.builder()
                                .withNumberOfTopTextLinesToDelete(0)
                                .build())
                        .withPagesPerDocument(1)
                        .build());

        List<Document> documents = pdfReader.get();
        var textSplitter = new TokenTextSplitter();
        vectorStore.accept(textSplitter.apply(documents));


        System.out.println("DOne processing");

    }

}
